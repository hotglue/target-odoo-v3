import json
import os

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


class UnifiedMapping:
    def __init__(self) -> None:
        pass

    def read_json_file(self, filename):
        # read file
        with open(os.path.join(__location__, f"{filename}"), "r") as filetoread:
            data = filetoread.read()

        # parse file
        content = json.loads(data)

        return content

    # Microsoft dynamics address mapping
    def map_address(self, address, address_mapping, payload):
        if isinstance(address, str):
            address = json.loads(address)

        if isinstance(address, dict):
            for key, value in address.items():
                if key in address_mapping.keys():
                    payload[address_mapping[key]] = value

        return payload

    def map_custom_fields(self, payload, fields):
        # Populate custom fields.
        for key, val in fields:
            payload[key] = val
        return payload

    def prepare_payload(self, record, endpoint="invoice"):
        mapping = self.read_json_file(f"mapping.json")
        ignore = mapping["ignore"]
        mapping = mapping[endpoint]
        payload = {}
        payload_return = {}
        lookup_keys = mapping.keys()
        for lookup_key in lookup_keys:
            if lookup_key == "address" or lookup_key == "addresses":
                payload = self.map_address(
                    record.get(lookup_key, []), mapping[lookup_key], payload
                )
            elif lookup_key == "phoneNumbers":
                payload = self.map_address(
                    record.get(lookup_key, []), mapping[lookup_key], payload
                )
            else:
                val = record.get(lookup_key, "")
                if val:
                    payload[mapping[lookup_key]] = val

        for key in payload.keys():
            if key not in ignore and key is not None:
                payload_return[key] = payload[key]
        return payload_return
